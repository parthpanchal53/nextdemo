// UI component will receive a single `post` object sent via the getSTaticprops function below

export default function Post({ post }) {
  return (
    <main>
      {post ? (
        <>
          <h1>Title : {post.title}</h1>
          <p>Body : {post.body}</p>
        </>
      ) : (
        "No title"
      )}
    </main>
  );
}

// This method constructs all the dynamic paths identified by a unique identifier (post.id in this case)
export async function getStaticPaths() {
  const posts = await fetch("https://jsonplaceholder.typicode.com/posts").then(
    (res) => res.json()
  );

  const paths = posts.map((post) => ({
    params: { id: post.id.toString() },
  }));

  console.log(paths);

  return {
    paths, // lookup the format in docs
    fallback: false, // look this up in the docs
  };
}

// In params , we can access the params.id (ie the id generated in the above function and fetch just that specific post in the function below and pass it to the UI component above)
export async function getStaticProps({ params }) {
  const post = await fetch(
    `https://jsonplaceholder.typicode.com/posts/${params.id}`
  ).then((res) => res.json());

  return { props: { post } };
}
