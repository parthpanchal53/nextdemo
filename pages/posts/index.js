import Link from "next/link";

export default function AllPosts({ posts }) {
  // Here posts is obtained from the getStaticProps function below .
  return (
    <main>
      <h1 className="title">All Posts</h1>

      {posts?.length > 0
        ? posts.map((post) => (
            <>
              <h1>{post.title}</h1>
              <Link href={`posts/${post.id}`}>
                <a>Go to Post</a>
              </Link>
            </>
          ))
        : null}
    </main>
  );
}

// Fetch a list of posts from the FAKE API point and pass it as props to the above AllPosts component
export async function getStaticProps() {
  const posts = await fetch("https://jsonplaceholder.typicode.com/posts").then(
    (res) => res.json()
  );

  console.log(posts); // This will have a list of all posts

  return { props: { posts } }; // Pass to the above Ui component
}
